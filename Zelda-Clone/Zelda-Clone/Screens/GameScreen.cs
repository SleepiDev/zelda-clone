﻿using Microsoft.Xna.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zelda_Clone.Screens
{
   public abstract class GameScreen
    {

        public bool hidden=false,persistant=false;

        abstract public void Initialize();

        abstract public void Draw(GameTime gameTime);
        abstract public void Update(GameTime gameTime);
         
        virtual protected void LoadContent(GameTime gameTime){ }
        virtual protected void UnloadContent(GameTime gameTime) { }

    }
}
