﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Zelda_Clone.Screens
{
    public class ScreenManager 
    {
        #region Properties and get/setters

        struct NextScreen
        {
            GameScreen screen;
            bool hidden, persistant, removeFromList;
            ScreenTransitions transition;


            public NextScreen(GameScreen next,ScreenTransitions _transition, bool _removeFromList, bool _hidden, bool _persistant)
            {
                screen = next;
                removeFromList = _removeFromList;
                hidden = _hidden;
                persistant = _persistant;
                transition = _transition;
            }
        }

        List<GameScreen> screens = new List<GameScreen>();
        List<GameScreen> screensToUpdate = new List<GameScreen>();
        List<GameScreen> screensToDraw = new List<GameScreen>();

        ContentManager content;
        SpriteBatch spriteBatch;
        SpriteFont font;
        NextScreen nextScreen;
        Game _game;

        //used for the different transitions
        public enum ScreenTransitions {SCREEN_NONE, SCREEN_FADE};
        public Game game
        {
            get {
                return _game;
                }
        }
        public GraphicsDevice GraphicsDevice
        {
            get
            {
                return game.GraphicsDevice;
            }
        }
        public GraphicsDeviceManager graphics
        {
            get
            {
                return graphics;
            }
        }
        //Careful with this because it never get's unloaded
        public ContentManager Content
        {
            get { return content; }
        }
        //Sharing the Spritebatch
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        bool transitioning;
        float screenAlpha = 0.0F;

        #endregion

        public ScreenManager(Game game, GraphicsDeviceManager graphics)
        {
            _game = game;
            content = new ContentManager(game.Services, "Content");
            init();
        }

        #region init and (un)load content
        protected void init()
        {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
            screens.Add(new Screen_SplashScreen(this));
        }

        protected void LoadContent()
        {

        }

        protected void UnloadContent()
        {
            
        }
        #endregion

        #region Draw And Update
        public void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            //Probably should only do this when Screens Change
            foreach (GameScreen screen in screens) {
               if (!screen.hidden)
                {
                    screensToDraw.Add(screen);
                }
            }

            foreach (GameScreen screen in screensToDraw)
            {
                screen.Draw(gameTime);
                
            }

            screensToDraw.Clear();
            spriteBatch.End();
        }

        public void Update(GameTime gameTime)
        {
            //Probably should only do this when Screens Change
            foreach (GameScreen screen in screens)
            {
                if (!screen.hidden)
                {
                    screensToUpdate.Add(screen);
                }
            }

        }

        public void QueueScreen(GameScreen screenToChange, ScreenTransitions transition, bool removeFromList = true, bool hide=true,bool persist=false)
        {
            
            if (transition != ScreenTransitions.SCREEN_NONE)
            {
                transitioning = true;
                nextScreen = new NextScreen(screenToChange, transition, removeFromList, hide, persist);
            }
            
        }

        #endregion  

    }
}